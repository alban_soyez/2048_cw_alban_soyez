import tkinter as tk
from textual_2048 import *
from math import floor
import time
from functools import partial
import play_2048

class Graph :
    """Contient les données relatives à l'apparence des cases, et permet d'afficher la grille dans une fenêtre"""
    background_color = '#92877d'
    empty_cell_color = '#9e948a'
    cell_background_colors = {
        '2': '#eee4da',
        '4': '#ede0c8',
        '8': '#f2b179',
        '16': '#f59563',
        '32': '#f67c5f',
        '64': '#f65e3b',
        '128': '#edcf72',
        '256': '#edcc61',
        '512': '#edc850',
        '1024': '#edc53f',
        '2048': '#edc22e',
        'beyond': '#3c3a32'
    }
    cell_colors = {
        '2': '#776e65',
        '4': '#776e65',
        '8': '#f9f6f2',
        '16': '#f9f6f2',
        '32': '#f9f6f2',
        '64': '#f9f6f2',
        '128': '#f9f6f2',
        '256': '#f9f6f2',
        '512': '#f9f6f2',
        '1024': '#f9f6f2',
        '2048': '#f9f6f2',
        'beyond': '#f9f6f2'
    }
    font = ('Verdana', 24, 'bold')
    up_keys = ('z', 'Z', 'Up')
    left_keys = ('q', 'Q', 'Left')
    down_keys = ('s', 'S', 'Down')
    right_keys = ('d', 'D', 'Right')

    def __init__(self,grid):
        self.size = len(grid[0])
        self.grid = grid
        self.window_2048 = tk.Tk()
        self.window_2048.title('2048')
        self.background = tk.Frame(self.window_2048, bg=Graph.background_color)
        self.cell_labels = []
        for k in range(self.size):
            row_labels = []
            for i in range(self.size):
                label = tk.Label(self.background, text='',justify=tk.CENTER, font=Graph.font,width=4, height=2)
                label.grid(row=k, column=i, padx=10, pady=10)
                row_labels.append(label)
            self.cell_labels.append(row_labels)
        self.background.pack()

    def display_and_update_graphical_grid(self):
        for k in range(self.size):
            for i in range(self.size):
                if self.grid[k][i] == '':
                    self.cell_labels[k][i].configure(text='',bg=Graph.empty_cell_color)
                else:
                    if int(self.grid[k][i]) > 2048:
                        bg_color = Graph.cell_background_colors.get('beyond')
                        fg_color = Graph.cell_colors.get('beyond')
                    else:
                        bg_color = Graph.cell_background_colors.get(self.grid[k][i])
                        fg_color = Graph.cell_colors.get(self.grid[k][i])
                    self.cell_labels[k][i].configure(
                        text=self.grid[k][i],
                        bg=bg_color, fg=fg_color)


class Game :
    """Permet de jouer"""
    def __init__(self, grid, panel):
        self.grid = grid
        self.panel = panel

    def start(self):
        self.panel.display_and_update_graphical_grid()
        self.panel.window_2048.bind('<Key>', self.key_handler)
        self.panel.window_2048.focus_force() # passe le jeu au premier plan et avec le focus pour les touches
        self.panel.window_2048.mainloop()


    def key_handler(self,event):
        key_value = event.keysym
        move = ''
        if key_value in Graph.up_keys:
            move = 'h'  #pour être cohérent avec les variables des autres fichiers
        elif key_value in Graph.left_keys:
            move = 'g'
        elif key_value in Graph.down_keys:
            move = 'b'
        elif key_value in Graph.right_keys:
            move = 'd'
        elif key_value == "Return":
            insert_random_tile(self.grid)
        else:
            pass
        self.panel.display_and_update_graphical_grid()
        if move != '':
            if move_possible(self.grid)[move]:
                move_grid(move,self.grid)
                self.panel.display_and_update_graphical_grid()
        if not move_possible(self.grid)['one_move']:
            self.end_of_the_game()

    def end_of_the_game(self):
        if get_grid_tile_max(self.grid) >= 2048 :
            label_won = tk.Label(self.panel.window_2048, text='Vous avez gagné avec un score de {} ! Voulez-vous rejouer ?'.format(get_grid_tile_max(self.grid)))
            label_won.pack()
        else :
            label_lost = tk.Label(self.panel.window_2048, text='Vous avez perdu avec un score de {} ! Voulez-vous rejouer ?'.format(get_grid_tile_max(self.grid)))
            label_lost.pack()
        button_play_again = tk.Button(self.panel.window_2048, text = 'Rejouer', command = self.play_again_2048)
        button_play_again.pack()

    def play_again_2048(self):
        self.panel.window_2048.destroy()
        play_2048()


def play_2048_alter():
    config = Config()
    size = config.enter_grid_size()
    grid = create_grid(size)
    insert_random_tile(grid)
    insert_random_tile(grid)
    panel = Graph(grid)
    game2048 = Game(grid,panel)
    game2048.start()

class Config :
    def __init__(self):
        pass

    def what_game(self):
        self.root = tk.Tk()
        label_0 = tk.Label(self.root, text = 'A quel jeu voulez-vous jouer ?')
        button_2048 = tk.Button(self.root, text = '2048 Classique', command = play_2048.play_2048)
        button_2048_alter = tk.Button(self.root, text = '2048 Alternatif', command = play_2048_alter )
        label_0.pack()
        button_2048.pack()
        button_2048_alter.pack()
        self.root.mainloop()

    def enter_grid_size(self):
        self.config_window = tk.Tk()
        self.config_window.focus_force() # met la fenêtre de config au 1er plan
        self.label = tk.Label(self.config_window, text = "Entrez la taille de la grille :")
        self.entry = tk.Entry(self.config_window)
        self.entry.focus_set() # actionne le curseur
        self.button = tk.Button(self.config_window, text = 'Valider', command = self.get_size)
        self.entry.bind("<Return>", self.get_size)
        self.label.pack()
        self.entry.pack()
        self.button.pack()
        self.config_window.mainloop()
        return self.size

    def get_size(self, args=None):
        self.size = int(self.entry.get())
        self.config_window.destroy()



if __name__ == '__main__':
    config = Config()
    config.what_game()






