from move_2048 import *
from grid_2048 import *

def is_grid_full(grid):
    size = len(grid[0])
    for k in range(size):
        for i in range(size):
            if grid[k][i] == '':
                return False
    return True


def move_left_possible_line(list):
    size = len(list)
    i=0
    while i<size and list[size-(i+1)] == '':
        i+=1
    queue = i #nombre de cases vides en fin de ligne
    for k in range(size-queue-1):
        if list[k] == '':
            return True
        if list[k] == list[k+1]:
            return True
    return False


def move_left_possible_grid(grid):
    size = len(grid[0])
    for k in range(size):
        if move_left_possible_line(grid[k]):
            return True
    return False


def move_right_possible_grid(grid):
    size = len(grid[0])
    for k in range(size):
        list = grid[k][:]
        list.reverse()
        if move_left_possible_line(list):
            return True
    return False

def move_up_possible_grid(grid):
    size = len(grid[0])
    for k in range(size):
        column = extract_column(grid,k)
        if move_left_possible_line(column):
            return True
    return False

def move_down_possible_grid(grid):
    size = len(grid[0])
    for k in range(size):
        column = extract_column(grid,k)
        column.reverse()
        if move_left_possible_line(column):
            return True
    return False

def move_possible(grid):
    allowed = {}
    allowed['g'] = move_left_possible_grid(grid)
    allowed['d'] = move_right_possible_grid(grid)
    allowed['h'] = move_up_possible_grid(grid)
    allowed['b'] = move_down_possible_grid(grid)
    allowed['one_move'] = allowed['g'] or allowed['d'] or allowed['b'] or allowed['h']
    return allowed


def get_grid_tile_max(grid):
    tiles = get_all_tiles(grid)
    return max(tiles)

def is_game_over(grid):
    if not move_possible(grid)['one_move']:
        # if get_grid_tile_max(grid) >= 2048 :
            # print('Vous avez gagné !')
        # else :
            # print('Vous avez perdu !')
        return True
    else :
        # print('La partie continue.')
        return False

