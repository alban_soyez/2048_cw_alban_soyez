import random

def create_grid(size): #crée une grille vide
    grid = [['' for _ in range (size)]for _ in range (size)]
    return grid

def insert_random_tile(grid): #insère un 2 ou un 4 sur une case vide, si elle existe
    size = len(grid[0])
    empty_tiles = get_empty_tiles(grid)
    nbr = len(empty_tiles)
    if nbr == 0 :
        return grid  #s'il n'y a aucune case vide
    else :
        k,i = empty_tiles[random.randrange(0,nbr)] #on choisit une case au hasard
        grid_add_new_tile_at_position(grid,k,i)
    return grid

def grid_add_new_tile_at_position(grid,k,i): #insère un 2 ou un 4 sur une case donnée
    grid[k][i] = rdm_tile()
    return grid

def rdm_tile():  #renvoie 2 avec 90% de chances et 4 avec 10% de chances
    if random.random() < 0.9:
        return '2'
    return '4'

def get_empty_tiles(grid): #renvoie la liste des tuples d'indices correspondant aux cases vides
    size = len(grid[0])
    empty_tiles = []
    for k in range (size):
        for i in range (size):
            if grid[k][i] == '':
                empty_tiles.append((k,i))
    return empty_tiles

def grid_to_string(grid): #affiche dans la console la grille du jeu
    n = longvalue(grid)
    size = len(grid[0])
    str = ""
    for k in range(size):
        str += " "
        for _ in range (size):
            str += n*"=" + " "
        str += '\n'
        str += '|'
        for i in range(size):
            diff = n-len(grid[k][i])
            space_left = diff //2
            str+= space_left * " "
            str += grid[k][i]
            space_right = (diff+1)//2
            str += space_right * " "
            str += '|'
        str += '\n'
    str += " "
    for _ in range (size):
        str += n*"=" + " "
    str += '\n'
    print(str)


def longvalue(grid): #renvoie la plus grande case dans la grille
    max = 0
    size = len(grid[0])
    for k in range(size):
        for i in range(size):
            if len(grid[k][i]) > max :
                max = len(grid[k][i])
    return max

def get_all_tiles(grid): #renvoie la liste de toutes les cases avec doublons
    size = len(grid[0])
    tiles = []
    for k in range(size):
        for i in range(size):
            if grid[k][i]!='':
                tiles.append(int(grid[k][i]))
            else :
                tiles.append(0)
    return tiles







