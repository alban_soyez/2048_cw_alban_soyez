from grid_2048 import *

def move_grid(move,grid):
    if move == 'g':
        move_left(grid)
    if move == 'd':
        move_right(grid)
    if move == 'h':
        move_up(grid)
    if move == 'b':
        move_down(grid)

def move_left(grid):
    size = len(grid[0])
    for k in range (size): #k parcourt les lignes
        fall_left(grid[k])
        merge_left(grid[k])
        fall_left(grid[k])
    return grid

def move_right(grid):
    size = len(grid[0])
    for k in range (size): #k parcourt les lignes
        grid[k].reverse()
        fall_left(grid[k])
        merge_left(grid[k])
        fall_left(grid[k])
        grid[k].reverse()
    return grid

def move_up(grid):
    size = len(grid[0])
    for i in range(size): #i parcourt les colonnes
        column = extract_column(grid,i)
        fall_left(column)
        merge_left(column)
        fall_left(column)
        grid = replace_column(grid,i,column)
    return grid

def move_down(grid):
    size = len(grid[0])
    for i in range(size): #i parcourt les colonnes
        column = extract_column(grid,i)
        column.reverse()
        fall_left(column)
        merge_left(column)
        fall_left(column)
        column.reverse()
        grid = replace_column(grid,i,column)
    return grid

def extract_column(matrix,i): #extrait la colonne i de matrix et la renvoie sous forme de liste
    column = []
    for k in range(len(matrix[0])):
        column.append(matrix[k][i])
    return column

def replace_column(matrix,i,column):  #remplace la colonne i de matrix par column
    for k in range(len(matrix[0])):
        matrix[k][i] = column[k]
    return matrix

def merge_left(list): #modifie list
    i = 0
    size = len(list)
    while i<(size-1) :
        if list[i] != '' and list[i] == list[i+1]:
            number_in_tile = int(list[i])
            list[i] = str(2*number_in_tile)
            list[i+1] = ''
            i+=2
        else : i+=1
    return list

def fall_left(list): #modifie list
    i = 0
    size = len(list)
    while i<size :   # i parcourt la liste
        current_strike = 0
        while i+current_strike < size and list[i+current_strike] == '' :
            current_strike+=1    # current_strike est le nombre de cases vides d'affilée à partir de la case i
        if i+current_strike == size :
            break    # il n'y a que des cases vides jusqu'à la fin de la ligne
        if current_strike != 0 :     # si current_strike = 0, aucun décalage à faire
            x = i     # x parcourt toutes les cases à partir de i
            while x < size :
                if x+current_strike < size :
                    list[x] = list[x+current_strike]  #on décale toutes les cases de current_strike vers la gauche
                else :
                    list[x] = ''   #les dernières cases sont vides
                x+=1
        i+=1
    return list







