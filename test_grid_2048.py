from pytest import *
from grid_2048 import *

def test_create_grid():
    assert create_grid() == [['','','',''],['','','',''],['','','',''],['','','','']]
    # assert create_grid() == [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]

def test_insert_random_tile():
    game_grid = create_grid()

def test_grid_add_new_tile_at_position():
    grid = create_grid(4)
    grid = grid_add_new_tile_at_position(grid,1,1)
    # assert grid==[['','','',''],['','4','',''],['','','',''],['','','','']] or grid==[['','','',''],['','2','',''],['','','',''],['','','','']]
    tiles = get_all_tiles(grid)
    assert 4 in tiles or 2 in tiles

