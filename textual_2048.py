from grid_2048 import *
from move_2048 import *
from game_over_2048 import *


def read_player_command(): #demande au joueur de jouer un coup
    move = input ("Entrez votre commande (g = gauche, d = droit, h = haut, b = bas) :")
    if move in ['g','d','b','h','stop'] :
        return move
    else :
        print ('Commande interdite, veuillez réessayer')
        return read_player_command()

def read_grid_size(): #demande la taille de la grille au joueur
    size = input("Entrez la taille de la grille :")
    try :
        val = int(size)
        return val
    except ValueError :
        print ('Taille incorrecte, veuillez réessayer')
        return read_grid_size()

def play():
    size = read_grid_size()
    grid = create_grid(size)
    insert_random_tile(grid)
    insert_random_tile(grid)
    grid_to_string(grid)
    while not is_game_over(grid) :
        move = read_player_command()
        if move == 'stop':
            break
        print(move_possible(grid)[move])
        if move_possible(grid)[move]:
            move_grid(move,grid)
            insert_random_tile(grid)
            grid_to_string(grid)
        print(grid)

if __name__ == '__main__':
    play()
    # exit(1)



