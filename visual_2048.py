import tkinter as tk
from functools import partial


# window = Tk()
# label_field = Label(window,text='Hello world !')
# label_field.pack()
# window.mainloop()

#
# def write_text():
#     print ('Hello Ciril')
#
# root = tk.Tk()
# frame = tk.Frame(root)
# frame.pack()
#
# button = tk.Button(frame,text="QUIT",activebackground="blue",fg="red",command=quit)
# button.pack(side=tk.LEFT)
# slogan = tk.Button(frame, fg="blue",text="Hello",command=write_text)
# slogan.pack(side = tk.LEFT)
#
# root.mainloop()


def update_label(label, stringvar):
    text = stringvar.get()
    label.config(text=text)
    stringvar.set('merci')

root = tk.Tk()
text = tk.StringVar(root)
label = tk.Label(root, text = 'Your name')
entry_name = tk.Entry(root, textvariable = text)
button = tk.Button(root, text='clic', command = partial(update_label, label, text))
label.grid(column=0, row=0)
entry_name.grid(column=0, row=1)
button.grid(column=0, row=2)

root.mainloop()